#include "MainScene.h"
#include "GameScene.h"
#include "Hero.h"
#include <sstream>

using namespace std;

cocos2d::Scene * GameScene::createScene()
{
	auto scene = Scene::create();
	auto layer = GameScene::create();
	scene->addChild(layer);
	return scene;
}

bool GameScene::init()
{
	if (!Layer::init())
	{
		return false;
	}

	Device::setAccelerometerEnabled(true);

	visibleSize = Director::getInstance()->getVisibleSize();
	visibleOrigin = Director::getInstance()->getVisibleOrigin();

	bestScore = UserDefault::getInstance()->getIntegerForKey("bestScore", 0);
	aData = UserDefault::getInstance()->getFloatForKey("a", 0);

	gameState = GAME_STATE_PLAYING;

	//background
	Sprite * bkg = Sprite::create("bkg.png");
	Size bkgSize = bkg->getContentSize();
	bkg->setAnchorPoint(Vec2(0.5, 0));
	bkg->setScale(640 / bkgSize.width);
	bkg->setPosition(visibleSize.width / 2, 0);
	bkg->setTag(TAG_BKG);
	this->addChild(bkg);

	//plats
	auto plats = Node::create();
	plats->setTag(TAG_PLATS);
	plats->setPosition(0, 0);
	this->addChild(plats);

	for (int i = 0; i < 5; i++)
	{
		auto plat = Sprite::create("plat0.png");
		plat->setScale(114 / plat->getContentSize().width);
		plat->setPositionX(visibleOrigin.x + 64 + 128 * i);
		plat->setPositionY(visibleOrigin.y + 100);
		plats->addChild(plat);
	}


// 	//hero1
// 	auto hero = Sprite::create("likright_X.png");
// 	hero->setPosition(visibleOrigin.x + visibleSize.width / 2, visibleOrigin.y + visibleSize.height / 2);
// 	hero->setTag(TAG_HERO);
// 	this->addChild(hero);

	//hero2
	auto hero = Hero::create();
	hero->setPosition(visibleOrigin.x + visibleSize.width / 2, visibleOrigin.y - 300);
	hero->setTag(TAG_HERO);
	this->addChild(hero);


	//score
	score = 0;
	auto scoreLabel = Label::createWithTTF("score:0", "fonts/font0.ttf", 56);
	scoreLabel->setAnchorPoint(Vec2(0, 1));
	scoreLabel->setPosition(visibleOrigin.x, visibleOrigin.y + visibleSize.height);
	scoreLabel->setTag(TAG_SCORE);
	scoreLabel->setColor(Color3B(0, 0, 0));
	this->addChild(scoreLabel, 6);

// 	//Chinese test ------fail
// 	auto t = Label::createWithTTF("����", "fonts/font0.ttf", 56);
// 	t->setPosition(visibleOrigin.x + 50, visibleOrigin.y + visibleSize.height - 100);
// 	t->setColor(Color3B(0, 0, 0));
// 	this->addChild(t);

	//stop button
	auto stopButton = MenuItemImage::create("stop_button.png", "stop_button.png", "stop_button_disable.png", CC_CALLBACK_1(GameScene::gamePause, this));
	stopButton->setScale(52 / stopButton->getContentSize().width);
	stopButton->setAnchorPoint(Vec2(1, 1));
	stopButton->setPosition(visibleOrigin.x + visibleSize.width, visibleOrigin.y + visibleSize.height);
	auto stopMenu = Menu::create(stopButton, NULL);
	stopMenu->setPosition(0, 0);
	stopMenu->setTag(TAG_STOP_MENU);
	this->addChild(stopMenu);
	

	//AccelerationListener
	auto listener = EventListenerAcceleration::create(CC_CALLBACK_2(GameScene::onAcceleration, this));
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

	//update
	scheduleUpdate();

	return true;
}

void GameScene::menuCloseCallback(Ref *pSender)
{
	Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	exit(0);
#endif
}

void GameScene::onAcceleration(Acceleration* acc, Event* unused_event)
{
	auto hero = (Hero*)this->getChildByTag(TAG_HERO);
	auto pos = hero->getPosition();
	float tmp = (acc->x - aData) * 30;
	if (tmp > 1)
	{
		hero->tern(RIGHT);
	}
	else if (tmp < -1)
	{
		hero->tern(LEFT);
	}

	if (pos.x + tmp > 640)
	{
		pos.x += (tmp - 640);
	}
	else if (pos.x + tmp < 0)
	{
		pos.x += (tmp + 640);
	}
	else
	{
		pos.x += tmp;
	}
	hero->setPosition(pos);
}

void GameScene::update(float delta)
{
	if (gameState == GAME_STATE_PLAYING)
	{
		//update(make and destroy) plat and other props
		auto plats = this->getChildByTag(TAG_PLATS);
		auto platList = plats->getChildren();
		Vector<Node*>::iterator it = platList.begin();

		float highest = 0;
		for (; it != platList.end(); it++)
		{
			float tmpH = (*it)->getPositionY();
			if (tmpH > highest)
			{
				highest = tmpH;
			}
			if (tmpH < -15)
			{
				plats->removeChild(*it);
			}
		}

		while (highest < 1200)
		{
			auto tmpPlat = Sprite::create("plat0.png");
			tmpPlat->setScale(114 / tmpPlat->getContentSize().width);
			highest += rand.random_int(0, 170) + 30;
			tmpPlat->setPosition(rand.random_int(0, 540) + 50, highest);
			//������ɵ���
			if (rand.random_int(0, 10) == 0)
			{
				
				auto spring = Sprite::create("spring0.png");
				spring->setAnchorPoint(Vec2(0.5, 0));
				spring->setPosition(17 + rand.random_int(0, (int)tmpPlat->getContentSize().width - 34), tmpPlat->getContentSize().height - 5);
				spring->setTag(TAG_SPRING);
				tmpPlat->addChild(spring);
			}
			plats->addChild(tmpPlat);
		}

		//update hero
		auto hero = (Hero*)this->getChildByTag(TAG_HERO);
		hero->update(plats);

		// is die by drop?
		auto heroPos = hero->getPosition();
		if (heroPos.y < 0 && hero->getSpeed() < 0)
		{
			auto stopMenu = (Menu*)this->getChildByTag(TAG_STOP_MENU);
			stopMenu->setEnabled(false);
			gameState = GAME_STATE_DEAD;
			this->goDieByDrop();
			return;
		}

		//move view and  update score
		
		if (heroPos.y > 600)
		{
			this->viewMoveUp(heroPos.y - 600);
			hero->setPositionY(600);

			//update score
			score += (heroPos.y - 600)/10;
			stringstream s;
			s << "score:" << int(score);
			auto scoreLabel = (Label*)this->getChildByTag(TAG_SCORE);
			scoreLabel->setString(s.str());
		}
	}
	
}

void GameScene::viewMoveUp(float pixels)
{
	auto plats = getChildByTag(TAG_PLATS);
	auto platList = plats->getChildren();
	for (Vector<Node*>::iterator it = platList.begin(); it != platList.end(); it++)
	{
		(*it)->setPositionY((*it)->getPositionY() - pixels);
	}
}

void GameScene::gamePause(Ref * pSender)
{
 	gameState = GAME_STATE_STOP;

	this->pause();

//  	auto stopMenu = (Menu*)this->getChildByTag(TAG_STOP_MENU);
//  	stopMenu->setEnabled(false);

	auto stopLayer = Layer::create();
	stopLayer->setPosition(0, 0);

	//�����²㰴ť
	auto listener = EventListenerTouchOneByOne::create();
	listener->setSwallowTouches(true);
	listener->onTouchBegan = CC_CALLBACK_2(GameScene::onTouchBegan, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, stopLayer);

	//��͸����
	auto translucent = Sprite::create("translucent.png");
	translucent->setScale(640 / translucent->getContentSize().width);
	translucent->setPosition(visibleOrigin.x + visibleSize.width / 2, visibleOrigin.y + visibleSize.height / 2);
	stopLayer->addChild(translucent);

	//pause_text
	auto pauseText = Sprite::create("paused_text.png");
	pauseText->setScale(261 / pauseText->getContentSize().width);
	pauseText->setPosition(visibleOrigin.x + visibleSize.width / 2, visibleOrigin.y + visibleSize.height / 2 + 100);
	stopLayer->addChild(pauseText);

	//resume button
	auto resumeButton = MenuItemImage::create("resume0.png", "resume1.png", CC_CALLBACK_1(GameScene::gameResume, this));
	resumeButton->setScale(224 / resumeButton->getContentSize().width);
	resumeButton->setPosition(visibleOrigin.x + visibleSize.width / 2, visibleOrigin.y + visibleSize.height / 2 - 100);
	auto resumeMenu = Menu::create(resumeButton, NULL);
	resumeMenu->setPosition(0, 0);
	stopLayer->addChild(resumeMenu);

	stopLayer->setTag(TAG_STOP_LAYAR);
	this->addChild(stopLayer, 10);
}

void GameScene::gameResume(Ref * pSender)
{
	this->resume();
	gameState = GAME_STATE_PLAYING;
// 	auto stopMenu = (Menu*)this->getChildByTag(TAG_STOP_MENU);
// 	stopMenu->setEnabled(true);

	this->removeChild(this->getChildByTag(TAG_STOP_LAYAR));

}

void GameScene::goDieByDrop()
{
	setBestScore();

	//hero's death action
	auto hero = this->getChildByTag(TAG_HERO);
	auto heroAc1 = MoveBy::create(0.5, Vec2(0, 600));
	auto heroAc2 = MoveBy::create(1, Vec2(0, -800));
	EaseIn::create(heroAc2, 1);
	hero->runAction(Sequence::create(heroAc1, EaseIn::create(heroAc2, 3), NULL));

	//plats action
	auto plats = this->getChildByTag(TAG_PLATS);
	auto platsAc = MoveBy::create(0.5, Vec2(0, 1200));
	plats->runAction(platsAc);

	//make after-dead menu and create action
	auto menuButton = MenuItemImage::create("menu_button0.png", "menu_button1.png", CC_CALLBACK_1(GameScene::goBackToMenu, this));
	menuButton->setScale(224 / menuButton->getContentSize().width);
	menuButton->setPosition(visibleOrigin.x + visibleSize.width / 2 - 150, visibleOrigin.y + visibleSize.height / 4);

	auto rePlayButton = MenuItemImage::create("playagain_button0.png", "playagain_button1.png", CC_CALLBACK_1(GameScene::rePlay, this));
	rePlayButton->setScale(224 / rePlayButton->getContentSize().width);
	rePlayButton->setPosition(visibleOrigin.x + visibleSize.width / 2 + 150, visibleOrigin.y + visibleSize.height / 4);

	auto afterDeadMenu = Menu::create(menuButton, rePlayButton, NULL);
	afterDeadMenu->setPosition(0, -1200);
	this->addChild(afterDeadMenu);

	auto menuAc1 = MoveBy::create(0.5, Vec2(0, 0));
	auto menuAc2 = MoveBy::create(1, Vec2(0, 1200));
	afterDeadMenu->runAction(Sequence::create(menuAc1, menuAc2, NULL));

	//text of "game over",score and bestScore
	auto textAftDe = Node::create();

	auto text_gameOver = Sprite::create("game_over_text.png");
	text_gameOver->setScale(430 / text_gameOver->getContentSize().width);
	text_gameOver->setPosition(visibleOrigin.x + visibleSize.width / 2, visibleOrigin.y + visibleSize.height * 2 / 3);
	textAftDe->addChild(text_gameOver);

	stringstream s;
	s << "your score:" << (int)score;
	auto text_score = Label::createWithTTF(s.str(), "fonts/font0.ttf", 56);
	text_score->setColor(Color3B(0, 0, 0));
	text_score->setPosition(visibleOrigin.x + visibleSize.width / 2, visibleOrigin.y + visibleSize.height / 2);
	textAftDe->addChild(text_score);

	s.str("");	//clear
	s << "best score:" << bestScore;
	auto text_bestScore = Label::createWithTTF(s.str(), "fonts/font0.ttf", 56);
	text_bestScore->setColor(Color3B(0, 0, 0));
	text_bestScore->setPosition(visibleOrigin.x + visibleSize.width / 2, visibleOrigin.y + visibleSize.height / 2 - 100);
	textAftDe->addChild(text_bestScore);

	textAftDe->setPosition(0, -1200);
	textAftDe->runAction(Sequence::create(menuAc1->clone(), menuAc2->clone(), NULL));
	this->addChild(textAftDe);

}

void GameScene::goBackToMenu(Ref * pSender)
{
	Director::getInstance()->popScene();
}

void GameScene::rePlay(Ref * pSender)
{
	Director::getInstance()->replaceScene(TransitionFade::create(0.5, GameScene::createScene()));
}

void GameScene::setBestScore()
{
	if (score > bestScore)
	{
		bestScore = score;
		UserDefault::getInstance()->setIntegerForKey("bestScore", bestScore);
	}
}

bool GameScene::onTouchBegan(Touch *touch, Event *unused_event)
{
	return true;
}

