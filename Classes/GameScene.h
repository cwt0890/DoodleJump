#ifndef _GAMESCENE_H_
#define _GAMESCENE_H_
#include "cocos2d.h"
USING_NS_CC;



#define GAME_STATE_PLAYING 0
#define GAME_STATE_STOP 1
#define GAME_STATE_DEAD 2

class GameScene :public Layer
{
private:
	int score;
	int bestScore;
	int gameState;
	Size visibleSize;
	Vec2 visibleOrigin;
	RandomHelper rand;
	float aData;
public:
	static Scene * createScene();
	virtual bool init();
	void menuCloseCallback(Ref *pSender);
	CREATE_FUNC(GameScene);
	void onAcceleration(Acceleration* acc, Event* unused_event);
	void update(float delta);

	void viewMoveUp(float pixels);
	void gamePause(Ref * pSender);
	void gameResume(Ref * pSender);
	void goDieByDrop();
	void goBackToMenu(Ref * pSender);
	void rePlay(Ref * pSender);
	void setBestScore();

 	bool onTouchBegan(Touch *touch, Event *unused_event);
};

#endif