#include "HelloWorldScene.h"
#include <sstream>
USING_NS_CC;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();



	Device::setAccelerometerEnabled(true);
	auto listener = EventListenerAcceleration::create(CC_CALLBACK_2(HelloWorld::onAcceleration, this));
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);


	auto bkg = Sprite::create("qwe.png");
	bkg->setScale(480 / bkg->getContentSize().width);
	bkg->setPosition(visibleSize.width / 2, visibleSize.height / 2);
	this->addChild(bkg);





    /////////////////////////////
    // 2. add a menu item with "X" image, which is clicked to quit the program
    //    you may modify it.

    // add a "close" icon to exit the progress. it's an autorelease object
//     auto closeItem = MenuItemImage::create(
//                                            "CloseNormal.png",
//                                            "CloseSelected.png",
//                                            CC_CALLBACK_1(HelloWorld::menuCloseCallback, this));
//     
// 	closeItem->setPosition(Vec2(origin.x + visibleSize.width - closeItem->getContentSize().width/2 ,
//                                 origin.y + closeItem->getContentSize().height/2));

    // create menu, it's an autorelease object
//     auto menu = Menu::create(closeItem, NULL);
//     menu->setPosition(Vec2::ZERO);
//     this->addChild(menu, 1);

    /////////////////////////////
    // 3. add your codes below...

    // add a label shows "Hello World"
    // create and initialize a label
    
//     auto label = Label::createWithTTF("Hello World", "fonts/Marker Felt.ttf", 24);
//     
//     // position the label on the center of the screen
//     label->setPosition(Vec2(origin.x + visibleSize.width/2,
//                             origin.y + visibleSize.height - label->getContentSize().height));
// 
//     // add the label as a child to this layer
//     this->addChild(label, 1);

    // add "HelloWorld" splash screen"
    auto sprite = Sprite::create("HelloWorld.png");
	sprite->setAnchorPoint(Vec2(0, 0));
	sprite->setPosition(0, 0);
	sprite->setTag(1);

    // position the sprite on the center of the screen
    //sprite->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));

    // add the sprite as a child to this layer
    this->addChild(sprite, 0);
    
	auto pos = sprite->getPosition();
	std::stringstream s;
	s << pos.x << "," << pos.y;
	auto l = Label::createWithTTF(s.str(), "fonts/Marker Felt.ttf", 24);
	l->setPosition(100, 100);
	this->addChild(l, 3);

    return true;
}


void HelloWorld::menuCloseCallback(Ref* pSender)
{
    Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}

void HelloWorld::onAcceleration(Acceleration* acc, Event* unused_event)
{
	auto sprite = (Sprite*)this->getChildByTag(1);
	Size size = sprite->getContentSize();
	Vec2 pos = sprite->getPosition();

	auto visibleSize = Director::getInstance()->getVisibleSize();
	auto origin = Director::getInstance()->getVisibleOrigin();

	if (pos.x + acc->x > origin.x + visibleSize.width)
	{
		pos.x = origin.x + visibleSize.width;
	}
	else if (pos.x + acc->x < origin.x)
	{
		pos.x = origin.x; 
	}
	else
		pos.x += acc->x;

	if (pos.y + acc->y > origin.y + visibleSize.height)
	{
		pos.y = origin.y + visibleSize.height;
	}
	else if (pos.y + acc->y < origin.y)
	{
		pos.y = origin.y;
	}
	else
		pos.y += acc->y;

	sprite->setPosition(pos);

}
