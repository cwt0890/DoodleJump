#ifndef _MAINSCENE_H_
#define _MAINSCENE_H_
#include "cocos2d.h"
using namespace cocos2d;

#define TAG_BKG 0
#define TAG_HERO 1
#define TAG_PLATS 2
#define TAG_SCORE 3
#define TAG_STOP_MENU 4
#define TAG_STOP_LAYAR 5
#define TAG_MENU 6
#define TAG_SPRING 7



class MainScene :public Layer
{
public:
	static Scene * createScene();
	virtual bool init();
	void menuCloseCallback(Ref* pSender);
	CREATE_FUNC(MainScene);
	void gameStart(Ref * pSender);
	void turnToOptionScene(Ref * pSender);

	bool onTouchBegan(Touch *touch, Event *unused_event);
	void onTouchMoved(Touch *touch, Event *unused_event);
	void onTouchEnded(Touch *touch, Event *unused_event);
	void update(float delta);

};





#endif