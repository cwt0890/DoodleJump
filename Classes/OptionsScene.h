#ifndef _OPTIONS_SCENE_H_
#define _OPTIONS_SCENE_H_
#include "cocos2d.h"
USING_NS_CC;

class OptionsScene:public Layer
{
public:
	float aData;
	float a;
public:
	static Scene * createScene();
	virtual bool init();
	void menuCloseCallback(Ref *pSender);
	CREATE_FUNC(OptionsScene);

	void setA(Ref *pSender);
	void backToMainScene(Ref *pSender);
	void onAcceleration(Acceleration* acc, Event* unused_event);
	void update(float delta);
};




#endif