#ifndef _HERO_H_
#define _HERO_H_
#include "cocos2d.h"

#define LEFT 0
#define RIGHT 1

USING_NS_CC;

class Hero :public Sprite
{
private:
	int faceTo;
	float speed, gravity;
public:
	virtual bool init();
	void menuCloseCallback(Ref *pSender);
	CREATE_FUNC(Hero);
	void update(Node* plats);
	void tern(int ft);
	void jump(float height,float times = 1);
	float getSpeed(){ return speed; }

};



#endif