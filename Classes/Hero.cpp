#include "Hero.h"
#include "MainScene.h"


bool Hero::init()
{
	if (!Sprite::init())
	{
		return false;
	}


	faceTo = RIGHT;
	gravity = 0.38f;
	speed = gravity*50;

	auto visibleSize = Director::getInstance()->getVisibleSize();
	auto visibleOrigin = Director::getInstance()->getVisibleOrigin();


	this->setTexture("likright_X.png");
	this->setScale(124 / getContentSize().width);
	

	return true;
}

void Hero::menuCloseCallback(Ref *pSender)
{
	Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	exit(0);
#endif
}

void Hero::update(Node* plats)
{
	auto heroPos = this->getPosition();

	if (faceTo == LEFT)
	{
		this->setTexture("likleft_X.png");
	}
	else if (faceTo == RIGHT)
	{
		this->setTexture("likright_X.png");
	}

	bool willJump = false;
	float jumpHeight;
	float times;
	Vector<Node*> v = plats->getChildren();
	for (Vector<Node*>::iterator it = v.begin(); it != v.end(); it++)
	{
		auto platPos = (*it)->getPosition();
		auto platSize = (*it)->getContentSize();
		
		auto vectorProps = (*it)->getChildren();
		if (!vectorProps.empty())
		{
			auto prop = (Sprite*)(*vectorProps.begin());
			auto propPos = prop->getPosition();
			if (prop->getTag() == TAG_SPRING)
			{
				//if (heroPos.x-propPos.x>-56&&heroPos.x-propPos.x<56&&heroPos.y-propPos.y>=85)
				if (heroPos.x - (platPos.x - platSize.width / 2 + propPos.x) > -56 && heroPos.x - (platPos.x - platSize.width / 2 + propPos.x) < 56 && heroPos.y - (platPos.y - platSize.height / 2 + propPos.y) >= 73 + 5 && heroPos.y + speed - (platPos.y - platSize.height / 2 + propPos.y) < 73 + 5)
				{
					jumpHeight = (platPos.y - platSize.height / 2 + propPos.y) + 73 + 5;
					times = 2;
					willJump = true;
					prop->setTexture("spring1.png");
					break;
				}
			}
		}
		if (heroPos.x - platPos.x < 70 && heroPos.x - platPos.x > -70 && heroPos.y - platPos.y >= 75 && heroPos.y + speed - platPos.y < 75)
		{
			willJump = true;
			jumpHeight = platPos.y + 75;
			times = 1;
			break;
		}
	}

	if (willJump)
		jump(jumpHeight, times);
	else
	{
		this->setPositionY(heroPos.y + speed);
		speed -= gravity;
	}
}

void Hero::jump(float height,float times)
{
	speed = gravity * 45 * times;
	//speed = -1;
	this->setPositionY(height);
}

void Hero::tern(int ft)
{
	faceTo = ft;
}

