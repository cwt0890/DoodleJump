#include "OptionsScene.h"
#include "MainScene.h"
#include "Hero.h"


Scene * OptionsScene::createScene()
{
	auto scene = Scene::create();
	auto layer = OptionsScene::create();
	scene->addChild(layer);
	return scene;
}

bool OptionsScene::init()
{
	if (!Layer::init())
	{
		return false;
	}

	Device::setAccelerometerEnabled(true);

	auto visibleSize = Director::getInstance()->getVisibleSize();
	auto visibleOrigin = Director::getInstance()->getVisibleOrigin();

	aData = UserDefault::getInstance()->getFloatForKey("a", 0);

	//background
	Sprite * bkg = Sprite::create("bkg.png");
	Size bkgSize = bkg->getContentSize();
	bkg->setAnchorPoint(Vec2(0.5, 0));
	bkg->setScale(640 / bkgSize.width);
	bkg->setPosition(visibleSize.width / 2, 0);
	bkg->setTag(TAG_BKG);
	this->addChild(bkg);

	//plats
	auto plats = Node::create();
	plats->setTag(TAG_PLATS);
	plats->setPosition(0, 0);
	this->addChild(plats);
	for (int i = 0; i < 5; i++)
	{
		auto plat = Sprite::create("plat0.png");
		plat->setScale(114 / plat->getContentSize().width);
		plat->setPositionX(visibleOrigin.x + 64 + 128 * i);
		plat->setPositionY(visibleOrigin.y + 200);
		plats->addChild(plat);
	}

	//hero2
	auto hero = Hero::create();
	hero->setPosition(visibleOrigin.x + visibleSize.width / 2, visibleOrigin.y);
	hero->setTag(TAG_HERO);
	this->addChild(hero);

	//menu
	auto setButton = MenuItemImage::create("set0.png", "set1.png", CC_CALLBACK_1(OptionsScene::setA, this));
	setButton->setScale(224 / setButton->getContentSize().width);
	setButton->setPosition(visibleOrigin.x + visibleSize.width / 4, visibleOrigin.y + visibleSize.height / 10);

	auto doneButton = MenuItemImage::create("done0.png", "done1.png", CC_CALLBACK_1(OptionsScene::backToMainScene, this));
	doneButton->setScale(224 / setButton->getContentSize().width);
	doneButton->setPosition(visibleOrigin.x + visibleSize.width * 3 / 4, visibleOrigin.y + visibleSize.height / 10);

	auto menu = Menu::create(setButton, doneButton, NULL);
	menu->setPosition(0, 0);
	this->addChild(menu);

	//AccelerationListener
	auto listener = EventListenerAcceleration::create(CC_CALLBACK_2(OptionsScene::onAcceleration, this));
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

	//update
	scheduleUpdate();

	return true;
}

void OptionsScene::menuCloseCallback(Ref *pSender)
{
	Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	exit(0);
#endif
}

void OptionsScene::setA(Ref *pSender)
{
	aData = a;
	UserDefault::getInstance()->setFloatForKey("a", aData);
}

void OptionsScene::backToMainScene(Ref *pSender)
{
	Director::getInstance()->popScene();
}

void OptionsScene::onAcceleration(Acceleration* acc, Event* unused_event)
{
	auto hero = (Hero*)this->getChildByTag(TAG_HERO);
	auto pos = hero->getPosition();
	a = acc->x;
	float tmp = (acc->x - aData) * 30;
	if (tmp > 1)
	{
		hero->tern(RIGHT);
	}
	else if (tmp < -1)
	{
		hero->tern(LEFT);
	}

	if (pos.x + tmp > 640)
	{
		pos.x += (tmp - 640);
	}
	else if (pos.x + tmp < 0)
	{
		pos.x += (tmp + 640);
	}
	else
	{
		pos.x += tmp;
	}
	hero->setPosition(pos);
}

void OptionsScene::update(float delta)
{
	auto plats = this->getChildByTag(TAG_PLATS);
	auto hero = (Hero*)this->getChildByTag(TAG_HERO);
	hero->update(plats);
}
