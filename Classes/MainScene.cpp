#include "MainScene.h"
#include "GameScene.h"
#include "OptionsScene.h"
#include "Hero.h"
#include <sstream>
using namespace std;


Scene * MainScene::createScene()
{
	auto scene = Scene::create();
	auto layer = MainScene::create();
	scene->addChild(layer);
	return scene;
}

bool MainScene::init()
{
	if (!Layer::init())
	{
		return false;
	}

	auto visibleSize = Director::getInstance()->getVisibleSize();
	auto visibleOrigin = Director::getInstance()->getVisibleOrigin();

	//background
	Sprite * bkg = Sprite::create("bkg.png");
 	Size bkgSize = bkg->getContentSize();
 	bkg->setAnchorPoint(Vec2(0.5, 0));
 	bkg->setScale(640 / bkgSize.width);
 	bkg->setPosition(visibleSize.width / 2, 0);
	bkg->setTag(TAG_BKG);
 	this->addChild(bkg);

// 	//Label test
// 	//string s;
// 	stringstream ss;
// 	ss << 123 << endl;
// 	ss << "ghfdgdfg" << endl;
// 	//ss.clear();	//不会清除数据
// 	ss.str("");		//用空字符串代替原来的数据
// 	ss << 234;
// 	auto label = Label::createWithTTF(ss.str(), "fonts/font0.ttf", 56);
// 	label->setPosition(visibleOrigin.x + visibleSize.width / 2, visibleOrigin.y + visibleSize.height - label->getContentSize().height / 2);
// 	this->addChild(label);


// 	//listener to move background
// 	auto listener = EventListenerTouchOneByOne::create();
// 	listener->onTouchBegan = CC_CALLBACK_2(MainScene::onTouchBegan, this);
// 	listener->onTouchMoved = CC_CALLBACK_2(MainScene::onTouchMoved, this);
// 	listener->onTouchEnded = CC_CALLBACK_2(MainScene::onTouchEnded, this);
// 	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

	//play button and options button
	auto palyButton = MenuItemImage::create("play0.png", "play1.png", CC_CALLBACK_1(MainScene::gameStart, this));
	palyButton->setPosition(visibleOrigin.x + visibleSize.width * 3 / 10, visibleOrigin.y + visibleSize.height * 17 / 24);
	palyButton->setScale(224 / palyButton->getContentSize().width);

	auto optionsButton = MenuItemImage::create("options0.png", "options1.png", CC_CALLBACK_1(MainScene::turnToOptionScene, this));
	optionsButton->setPosition(visibleOrigin.x + visibleSize.width * 3 / 4, visibleOrigin.y + visibleSize.height / 6);
	optionsButton->setScale(114 / optionsButton->getContentSize().width);

	auto menu = Menu::create(palyButton, optionsButton, NULL);
	menu->setPosition(0, 0);
	menu->setTag(TAG_MENU);
	this->addChild(menu);

	//doodlejump_text
	auto doodlejump_text = Sprite::create("doodlejump_text.png");
	doodlejump_text->setScale(400 / doodlejump_text->getContentSize().width);
	doodlejump_text->setAnchorPoint(Vec2(0, 0.5));
	doodlejump_text->setPosition(visibleOrigin.x + 20, visibleOrigin.y + visibleSize.height * 7 / 8);
	this->addChild(doodlejump_text);

	//monsters
	auto monster1 = Sprite::create("monster1.png");
	monster1->setScale(43 / monster1->getContentSize().width);
	monster1->setPosition(visibleOrigin.x + visibleSize.width / 5, visibleOrigin.y + visibleSize.height * 15 / 16);
	this->addChild(monster1);

	auto monster2 = Sprite::create("monster2.png");
	monster2->setScale(74 / monster2->getContentSize().width);
	monster2->setPosition(visibleOrigin.x + visibleSize.width * 4 / 5, visibleOrigin.y + visibleSize.height / 3);
	this->addChild(monster2);

	auto monster3 = Sprite::create("monster3.png");
	monster3->setScale(42 / monster3->getContentSize().width);
	monster3->setPosition(visibleOrigin.x + visibleSize.width / 7, visibleOrigin.y + visibleSize.height * 3 / 5);
	this->addChild(monster3);
	
	auto monster4 = Sprite::create("monster4.png");
	monster4->setScale(41 / monster4->getContentSize().width);
	monster4->setPosition(visibleOrigin.x + visibleSize.width * 3 / 5, visibleOrigin.y + visibleSize.height * 3 / 5);
	this->addChild(monster4);

	auto monster5 = Sprite::create("monster5.png");
	monster5->setScale(58 / monster5->getContentSize().width);
	monster5->setPosition(visibleOrigin.x + visibleSize.width * 3 / 5, visibleOrigin.y + visibleSize.height * 2 / 5);
	this->addChild(monster5);

	//hole
	auto hole = Sprite::create("hole.png");
	hole->setScale(148 / hole->getContentSize().width);
	hole->setPosition(visibleOrigin.x + visibleSize.width * 4 / 5, visibleOrigin.y + visibleSize.height / 2);
	this->addChild(hole);

	//UFO
	auto UFO = Sprite::create("UFO_normal.png");
	UFO->setScale(158 / UFO->getContentSize().width);
	UFO->setPosition(visibleOrigin.x + visibleSize.width * 4 / 5, visibleOrigin.y + visibleSize.height * 5 / 6);
	this->addChild(UFO);

	auto UFOAnimation = Animation::create();
	UFOAnimation->addSpriteFrameWithFile("UFO_normal.png");
	UFOAnimation->addSpriteFrameWithFile("UFO_with_light.png");
	UFOAnimation->setDelayPerUnit(0.1f);
	UFOAnimation->setLoops(10);
	auto UFOAnime = Animate::create(UFOAnimation);

	auto ac1 = MoveBy::create(1, Vec2(0, -100));
	auto ac2 = MoveBy::create(1, Vec2(0, 100));
	auto moveAction = Sequence::create(EaseSineInOut::create(ac1), EaseSineInOut::create(ac2), NULL);
	
	UFO->runAction(RepeatForever::create(Spawn::create(UFOAnime, moveAction, NULL)));

	//plat
	auto plats = Node::create();
	plats->setPosition(0, 0);
	auto plat = Sprite::create("plat0.png");
	plat->setScale(114 / plat->getContentSize().width);
	plat->setPosition(visibleOrigin.x + visibleSize.width / 5, visibleOrigin.y + visibleSize.height / 7);
	plats->addChild(plat);
	plats->setTag(TAG_PLATS);
	this->addChild(plats);

	//hero
	auto hero = Hero::create();
	hero->setPosition(visibleOrigin.x + visibleSize.width / 5, visibleOrigin.y - 200);
	hero->setTag(TAG_HERO);
	this->addChild(hero);

	scheduleUpdate();

	return true;
}

void MainScene::menuCloseCallback(Ref* pSender)
{
	Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	exit(0);
#endif
}

void MainScene::gameStart(Ref * pSender)
{
	Director::getInstance()->pushScene(TransitionFade::create(0.5, GameScene::createScene()));
}

bool MainScene::onTouchBegan(Touch *touch, Event *unused_event)
{
	auto loc = touch->getLocation();
	auto bkg = (Sprite*)this->getChildByTag(TAG_BKG);
	auto bkgSize = bkg->getContentSize();
	auto bkgPos = bkg->getPosition();
	return true;
}

void MainScene::onTouchMoved(Touch *touch, Event *unused_event)
{
	auto delta = touch->getDelta();
	auto bkg = (Sprite*)this->getChildByTag(TAG_BKG);
	auto bkgPos = bkg->getPosition();
	bkg->setPosition(bkgPos.x + delta.x, bkgPos.y + delta.y);
}

void MainScene::onTouchEnded(Touch *touch, Event *unused_event)
{
	;
}

void MainScene::update(float delta)
{
 	auto hero = (Hero*)this->getChildByTag(TAG_HERO);
 	hero->update(this->getChildByTag(TAG_PLATS));
}

void MainScene::turnToOptionScene(Ref * pSender)
{
	Director::getInstance()->pushScene(TransitionFade::create(0.5, OptionsScene::createScene()));
}


